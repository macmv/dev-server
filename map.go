package main

import (
  "image/color"
)

var colors = setupColors()

func setupColors() map[byte]color.Color {
  colors := make(map[byte]color.Color)
  // colors[0]  = color.RGBA{0  , 0  , 0  , 0}
  colors[1]  = color.RGBA{127, 178, 56 , 0}
  colors[2]  = color.RGBA{247, 233, 163, 0}
  colors[3]  = color.RGBA{199, 199, 199, 0}
  colors[4]  = color.RGBA{255, 0  , 0  , 0}
  colors[5]  = color.RGBA{160, 160, 255, 0}
  colors[6]  = color.RGBA{167, 167, 167, 0}
  colors[7]  = color.RGBA{0  , 124, 0  , 0}
  colors[8]  = color.RGBA{255, 255, 255, 0}
  colors[9]  = color.RGBA{164, 168, 184, 0}
  colors[10] = color.RGBA{151, 109, 77 , 0}
  colors[11] = color.RGBA{112, 112, 112, 0}
  colors[12] = color.RGBA{64 , 64 , 255, 0}
  colors[13] = color.RGBA{143, 119, 72 , 0}
  colors[14] = color.RGBA{255, 252, 245, 0}
  colors[15] = color.RGBA{216, 127, 51 , 0}
  colors[16] = color.RGBA{178, 76 , 216, 0}
  colors[17] = color.RGBA{102, 153, 216, 0}
  colors[18] = color.RGBA{229, 229, 51 , 0}
  colors[19] = color.RGBA{127, 204, 25 , 0}
  colors[20] = color.RGBA{242, 127, 165, 0}
  colors[21] = color.RGBA{76 , 76 , 76 , 0}
  colors[22] = color.RGBA{153, 153, 153, 0}
  colors[23] = color.RGBA{76 , 127, 153, 0}
  colors[24] = color.RGBA{127, 63 , 178, 0}
  colors[25] = color.RGBA{51 , 76 , 178, 0}
  colors[26] = color.RGBA{102, 76 , 51 , 0}
  colors[27] = color.RGBA{102, 127, 51 , 0}
  colors[28] = color.RGBA{153, 51 , 51 , 0}
  colors[29] = color.RGBA{25 , 25 , 25 , 0}
  colors[30] = color.RGBA{250, 238, 77 , 0}
  colors[31] = color.RGBA{92 , 219, 213, 0}
  colors[32] = color.RGBA{74 , 128, 255, 0}
  colors[33] = color.RGBA{0  , 217, 58 , 0}
  colors[34] = color.RGBA{129, 86 , 49 , 0}
  colors[35] = color.RGBA{112, 2  , 0  , 0}
  colors[36] = color.RGBA{209, 177, 161, 0}
  colors[37] = color.RGBA{159, 82 , 36 , 0}
  colors[38] = color.RGBA{149, 87 , 108, 0}
  colors[39] = color.RGBA{112, 108, 138, 0}
  colors[40] = color.RGBA{186, 133, 36 , 0}
  colors[41] = color.RGBA{103, 117, 53 , 0}
  colors[42] = color.RGBA{160, 77 , 78 , 0}
  colors[43] = color.RGBA{57 , 41 , 35 , 0}
  colors[44] = color.RGBA{135, 107, 98 , 0}
  colors[45] = color.RGBA{87 , 92 , 92 , 0}
  colors[46] = color.RGBA{122, 73 , 88 , 0}
  colors[47] = color.RGBA{76 , 62 , 92 , 0}
  colors[48] = color.RGBA{76 , 50 , 35 , 0}
  colors[49] = color.RGBA{76 , 82 , 42 , 0}
  colors[50] = color.RGBA{142, 60 , 46 , 0}
  colors[51] = color.RGBA{37 , 22 , 16 , 0}
  return colors
}

func rgb_to_map(col color.Color) byte {
  multipliers := []float64{0.71, 0.86, 1, 0.53}
  // multipliers := []float64{0.53}
  min_dist := -1.0
  closet_id := byte(0)
  val_r, val_g, val_b, _ := col.RGBA()
  r1 := float64(val_r) / 256 / 256
  g1 := float64(val_g) / 256 / 256
  b1 := float64(val_b) / 256 / 256
  for i := 0; i < 4; i++ {
    for id, other_color := range colors {
      other_r, other_g, other_b, _ := other_color.RGBA()
      r2 := (float64(other_r) / 256 / 256) * multipliers[i]
      g2 := (float64(other_g) / 256 / 256) * multipliers[i]
      b2 := (float64(other_b) / 256 / 256) * multipliers[i]

      // Used in weights
      rmean := (r1 + r2) / 2.0
      // Distance between rgb values. Values are squared, so we don't care if they are negative here
      r := r1 - r2
      g := g1 - g2
      b := b1 - b2
      // weights
      wr := 2 + rmean
      wg := 4.0
      wb := 2 + (1 - rmean)

      // Sum the square of all the colors multiplied by their weights
      dist := wr * r * r + wg * g * g + wb * b * b
      if dist < min_dist || min_dist == -1 {
        min_dist = dist
        closet_id = id * 4 + byte(i)
      }
    }
  }
  return closet_id
}
