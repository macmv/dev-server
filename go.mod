module gitlab.com/macmv/dev-server

go 1.15

replace gitlab.com/macmv/sugarcane => ../sugarcane

require (
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	gitlab.com/macmv/log v0.0.0-20210317223941-4b5f30cdc234
	gitlab.com/macmv/sugarcane v0.0.0-00010101000000-000000000000
)
