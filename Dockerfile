FROM sugarcane_proxy-build:latest AS build

WORKDIR /go/src/cubiness/dev-server

RUN apk update && apk upgrade && \
    apk add --no-cache gcc musl-dev

COPY . .

# Because there is no better way to do this,
# we must edit the go.mod every time.
RUN go mod edit -replace "gitlab.com/macmv/sugarcane=/go/src/cubiness/sugarcane" && \
  go get -d -v ./... && \
  go install -v ./...

# Blank image, for final build
FROM alpine:3.12 AS dev-server

WORKDIR /go/bin
COPY --from=build /go/bin/dev-server .
COPY --from=build /go/src/cubiness/dev-server/data ./data

EXPOSE 8483
ENTRYPOINT ["/go/bin/dev-server"]
