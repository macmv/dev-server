#! /bin/sh
# This builds and runs the dev server.

./build.sh

if go install -v; then
  dev-server $@
fi
