#! /bin/sh
# This compiles all protobufs for the server.

cd ../sugarcane

protoc proto/*.proto \
  --go_out=. \
  --go_opt=paths=source_relative \
  --go-grpc_out=. \
  --go-grpc_opt=paths=source_relative

cd ../dev-server
