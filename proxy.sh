#! /bin/sh
# This builds and runs the proxy.

./build.sh

cd ../sugarcane

if go install -v; then
  sugarcane -local $@
fi
