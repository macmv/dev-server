package main

import (
  "flag"
  "math"
  "strconv"
  "net/http"
  _ "net/http/pprof"

  "image"
  _ "image/png"
  _ "image/jpeg"

  "github.com/nfnt/resize"

  "gitlab.com/macmv/log"
  "gitlab.com/macmv/sugarcane/minecraft"
  "gitlab.com/macmv/sugarcane/minecraft/item"
  "gitlab.com/macmv/sugarcane/minecraft/util"
  "gitlab.com/macmv/sugarcane/minecraft/world"
  "gitlab.com/macmv/sugarcane/minecraft/entity"
  "gitlab.com/macmv/sugarcane/minecraft/packet"
  "gitlab.com/macmv/sugarcane/minecraft/player"
  "gitlab.com/macmv/sugarcane/minecraft/world/block"
  "gitlab.com/macmv/sugarcane/minecraft/event/command"

  pb "gitlab.com/macmv/sugarcane/proto"
)

var (
  flag_port = flag.String("port", ":8483", "The port that the server should run the grpc server on")
)

func main() {
  go func() {
    http.ListenAndServe("0.0.0.0:6060", nil)
  }()

  flag.Parse()

  // ALL THE LOGS
  log.GlobalLogger.Level = log.DEBUG

  mc := minecraft.New(false, 0, false, "world")
  mc.Init()

  mc.FinalizeBlocks()

  mc.Events.AddCommand(command.New("fill", handleFill,
    command.LiteralNode("circle",
      command.ArgumentNode("center", command.Parser(command.BLOCK_POS),
        command.ArgumentNode("radius", command.Parser(command.FLOAT).Min(0),
          command.ArgumentNode("block", command.Parser(command.BLOCK_STATE)),
        ),
      ),
    ),
  ))

  mc.Events.AddCommand(command.New("map", handleMap,
    command.ArgumentNode("url", command.Parser(command.STRING)),
  ))

  mc.Events.AddCommand(command.New("pokimane", handlePokimane))

  mc.Events.AddCommand(command.New("setblock", handleSetblock,
    command.ArgumentNode("position", command.Parser(command.BLOCK_POS),
      command.ArgumentNode("block", command.Parser(command.BLOCK_STATE)),
    ),
  ))

  mc.Events.AddCommand(command.New("give", handleGive,
    command.ArgumentNode("item_name", command.Parser(command.ITEM_STACK)),
  ))

  mc.Events.AddCommand(command.New("spawn", handleSpawn))
  mc.Events.AddCommand(command.New("test", handleSpawn))
  mc.On("block-change", placeBlock)

  mc.FinishLoad()
  // mc.WorldManager.SetResourcePack("https://download2188.mediafire.com/qgkki2pu9pcg/h4tdxcoboo8eomu/Luna32x+Free+pack.zip")

  mc.StartUpdateLoop(*flag_port)
}

func handleTest(world_manager *world.WorldManager, player *player.Player, args []string) bool {
  return true
}

func handleFill(world_manager *world.WorldManager, player *player.Player, args []string) bool {
  if len(args) == 0 {
    return false
  }
  if args[0] == "circle" {
    if len(args) != 6 {
      return false
    }
    x, y, z, err := command.ParsePosition(player, args[1], args[2], args[3])
    if err != nil {
      chat := util.NewChat()
      chat.AddSectionColor("Invalid location!", "red")
      player.SendChat(chat)
      return true
    }
    radius, err := command.ParseFloat(args[4])
    if err != nil {
      chat := util.NewChat()
      chat.AddSectionColor("Invalid radius!", "red")
      player.SendChat(chat)
      return true
    }
    kind := block.GetKind(args[5])
    if kind == nil {
      chat := util.NewChat()
      chat.AddSectionColor("Invalid block!", "red")
      player.SendChat(chat)
      return true
    }
    new_block := kind.DefaultType()
    world := world_manager.GetWorldOfPlayer(player)
    for x_off := int32(-radius) - 1; x_off < int32(radius) + 1; x_off++ {
      for z_off := int32(-radius) - 1; z_off < int32(radius) + 1; z_off++ {
        dist := float32(math.Sqrt(math.Pow(float64(x_off), 2) + math.Pow(float64(z_off), 2)))
        if dist < radius {
          world.SetBlock(block.Pos{x + x_off, y, z + z_off}, new_block)
        }
      }
    }
  }
  return true
}

func handleSetblock(world_manager *world.WorldManager, player *player.Player, args []string) bool {
  if len(args) < 4 {
    player.SendChat(util.NewChatFromStringColor("Please enter a block position and name", "red"))
    return false
  }
  x, y, z, err := command.ParsePosition(player, args[0], args[1], args[2])
  if err != nil {
    chat := util.NewChat()
    chat.AddSectionColor("Invalid location!", "red")
    player.SendChat(chat)
    return true
  }
  kind := block.GetKind(args[3])
  if kind == nil {
    chat := util.NewChat()
    chat.AddSectionColor("Invalid block!", "red")
    player.SendChat(chat)
    return true
  }
  new_block := kind.DefaultType()
  world := world_manager.GetWorldOfPlayer(player)
  world.SetBlock(block.Pos{x, y, z}, new_block)
  return true
}

func handleMap(world_manager *world.WorldManager, player *player.Player, args[]string)bool{
  if len(args) < 1 {
    player.SendChat(util.NewChatFromStringColor("Please enter a valid url!", "red"))
    return false
  }
  resp, err := http.Get(args[0])
  if err != nil {
    msg := util.NewChat()
    msg.AddSectionColor("Error while getting url: ", "red")
    msg.AddSectionColor(err.Error(), "white")
    player.SendChat(msg)
    return false
  }
  defer resp.Body.Close()

  im, _, err := image.Decode(resp.Body)
  if err != nil {
    msg := util.NewChat()
    msg.AddSectionColor("Error while decoding image: ", "red")
    msg.AddSectionColor(err.Error(), "white")
    player.SendChat(msg)
    return false
  }
  im = resize.Resize(128, 128, im, resize.Bilinear)

  map_data := pb.Map{}
  map_data.Id = 0
  map_data.Columns = 128
  map_data.Rows = 128
  map_data.Data = make([]byte, 128 *128)
  for z := 0; z < 128; z++ {
    for x := 0; x < 128; x++ {
      map_data.Data[z * 128 + x] = rgb_to_map(im.At(x, z))
    }
  }
  out := packet.NewOutgoingPacket(packet.Clientbound_MapData)
  out.SetOther(0, &map_data)
  out.Send(player.PacketStream)
  i := item.NewItemStack(item.GetItem("minecraft:filled_map"), 1, nil)
  out = packet.NewOutgoingPacket(packet.Clientbound_SetSlot)
  out.SetByte(0, 0)
  out.SetShort(0, 36)
  out.SetItem(0, i)
  out.Send(player.PacketStream)

  return true
}

func handleGive(world_manager *world.WorldManager, player *player.Player, args []string) bool {
  if len(args) < 1 {
    player.SendChat(util.NewChatFromStringColor("Please enter an item name", "red"))
    return false
  }
  it := item.GetItem(args[0])
  if it == nil {
    player.SendChat(util.NewChatFromStringColor("Please enter a valid item name", "red"))
    return false
  }
  stack := item.NewItemStack(it, 1, nil)
  player.Inventory().SetHotbarItem(8, stack)
  return true
}

func handleSpawn(world_manager *world.WorldManager, player *player.Player, args []string) bool {
  w := world_manager.GetWorldOfPlayer(player)
  val, _ := strconv.Atoi(args[0])
  e := w.Spawn(entity.Type(val), player.X(), player.Y(), player.Z(), 0, 0, 0, 0, 0, 0)
  log.Info("Spawn entity: ", e)
  return true
}

func handlePokimane(world_manager *world.WorldManager, player *player.Player, args []string) bool {
  chat := util.NewChat()
  chat.AddLinkColor("Pokimane's Twitch", "purple", "https://www.twitch.tv/pokimane")
  player.SendChat(chat)
  return true
}

func placeBlock(new_block *block.Type, w *world.World, block *block.State, player *player.Player) bool {
  handleFence(new_block, w, block)
  return false
}

func handleFence(new_block *block.Type, w *world.World, block *block.State) {
  handleFenceType("minecraft:oak_fence",      new_block, w, block)
  handleFenceType("minecraft:birch_fence",    new_block, w, block)
  handleFenceType("minecraft:spruce_fence",   new_block, w, block)
  handleFenceType("minecraft:jungle_fence",   new_block, w, block)
  handleFenceType("minecraft:acacia_fence",   new_block, w, block)
  handleFenceType("minecraft:dark_oak_fence", new_block, w, block)
}

func handleFenceType(fence string, new_block *block.Type, w *world.World, b *block.State) {
  if new_block.Name() == fence {
    p := b.Pos()
    p.X--
    if w.Block(p).Type().Name() == fence {
      w.SetBlockProperty(p, "east", "true")
      p.X++
      w.SetBlockProperty(p, "west", "true")
    }
    p = b.Pos()
    p.Z--
    if w.Block(p).Type().Name() == fence {
      w.SetBlockProperty(p, "south", "true")
      p.Z++
      w.SetBlockProperty(p, "north", "true")
    }
    p = b.Pos()
    p.X++
    if w.Block(p).Type().Name() == fence {
      w.SetBlockProperty(p, "west", "true")
      p.X--
      w.SetBlockProperty(p, "east", "true")
    }
    p = b.Pos()
    p.Z++
    if w.Block(p).Type().Name() == fence {
      w.SetBlockProperty(p, "north", "true")
      p.Z--
      w.SetBlockProperty(p, "south", "true")
    }
  } else {
    p := b.Pos()
    p.X--
    if w.Block(p).Type().Name() == fence {
      w.SetBlockProperty(p, "east", "false")
    }
    p = b.Pos()
    p.Z--
    if w.Block(p).Type().Name() == fence {
      w.SetBlockProperty(p, "south", "false")
    }
    p = b.Pos()
    p.X++
    if w.Block(p).Type().Name() == fence {
      w.SetBlockProperty(p, "west", "false")
    }
    p = b.Pos()
    p.Z++
    if w.Block(p).Type().Name() == fence {
      w.SetBlockProperty(p, "north", "false")
    }
  }
}
